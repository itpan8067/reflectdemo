# Reflect注解
> C/C++/Java不是动态语言，但是Java有一定动态性，可以利用反射机制，字节码操作获取类似动态语言特性。

## 反射机制的常见作用
- 动态加载类、动态获取类的信息(构造器，属性，方法)；
- 动态构造对象；
- 动态调用类和对象的任意方法、构造器；
- 动态调用和处理属性；
- 获取泛型信息；
- 处理注解；

## 反射 - 泛型
- Java采用泛型擦除机制来引入泛型，Java中的泛型仅仅是给编译器javac使用的，确保数据的安全性和免去强制类型转换的麻烦。但是，一旦编译完成，所有和泛型有关的类型全部擦除。
- 为了通过反射操作泛型，Java新增了ParameterizedType，GenericArrayType, TypeVariable和WildcardType来将这些泛型信息保存起来，以便反射可以获取。
    - ParameterizedType：表示一种参数化的类型。如：Collection<String>；
    - GenericArrayType：表示一种元素类型是参数话类型或者类型变量的数组类型；
    - TypeVariable：是各种类型变量的公共父接口；
    - WildcardType：代表一种通配符类型表达式，如：?, ? extend Number, ? super Integer；