package com.pc.reflect;

import com.pc.reflect.bean.Person;

/**
 * @author panchao
 * @create 2019/4/18 17:46
 * @Describe
 *      反射 -- Class使用
 * java.lang.Class是反射的源头
 * 我们创建一个类，通过编译(javac.exe)，生成对应的.class文件，之后我们使用java.exe
 * (JVM的类加载器完成的)加载.class文件，此.class文件加载到内存以后，就是一个运行时类
 * 存在缓存区，那么这个运行时类本身就是一个Class的实例。
 * 1. 每一个运行时类只加载一次。
 * 2. 获取Class的实例之后，我们就可以：
 *      1) 创建对应的运行时类的对象；
 *      2) 获取对应的运行时类的完整结构(属性，方法，构造器，内部类，父类，包，异常，注解等)；
 *      3) 调用对应的运行时类的指定的结构(属性，方法，构造器等)；
 *      4) 反射动态代理；
 */
public class Test2 implements ITest {

    @Override
    public void test(){
        try {
            // 1. 调用运行时类本身的.class属性
            System.out.println("====================================");
            System.out.println("获取Class的实例4种常见方法");
            System.out.println("====================================");
            Class clazz1 = Person.class;
            System.out.println("1. 调用运行时类本身的.class属性 -- Person.class  =  " + clazz1.getName());

            // 2. 调用运行时类的对象
            Person person = new Person();
            Class clazz2 = person.getClass();
            System.out.println("2. 调用运行时类的对象 -- person.getClass()  =  " + clazz2.getName());

            // 3. 通过Class静态方法forName
            Class clazz3 = Class.forName("com.example.cameratest.test.bean.Person");
            System.out.println("3. 通过Class静态方法forName -- Class.forName()  =  " + clazz3.getName());


            //4. 通过ClassLoader类加载器
            ClassLoader classLoader = this.getClass().getClassLoader();
            Class clazz4 = classLoader.loadClass("com.pc.reflect.bean.Person");
            System.out.println("4. 通过ClassLoader类加载器 -- ClassLoader  =  " + clazz4.getName());

            System.out.println(clazz1 == clazz2);
            System.out.println(clazz1 == clazz2);
            System.out.println(clazz1 == clazz3);
            System.out.println(clazz1 == clazz4);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

    }

}
