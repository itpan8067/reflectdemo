package com.pc.reflect;

import com.pc.reflect.bean.People;

import java.lang.annotation.Annotation;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

/**
 * @author panchao
 * @create 2019/4/19 18:39
 * @Describe
 *      反射 -- 接口，注解，父类等操作
 */
public class Test5 implements ITest {

    @Override
    public void test(){
        Class clazz = People.class;

        System.out.println("============================================");
        System.out.println("getSuperclass()：获取父类的运行时的Class");
        System.out.println("============================================");
        Class superClass = clazz.getSuperclass();
        System.out.println("getSuperclass() = " + superClass.toString());

        System.out.println("\n");
        System.out.println("============================================");
        System.out.println("getGenericSuperclass()：获取带泛型父类的运行时的Class");
        System.out.println("============================================");
        Type genericSuperclass = clazz.getGenericSuperclass();
        System.out.println("getGenericSuperclass() = " + genericSuperclass.toString());

        System.out.println("\n");
        System.out.println("============================================");
        System.out.println("getGenericSuperclass()：获取父类的泛型类");
        System.out.println("============================================");
        Type type = clazz.getGenericSuperclass();
        // 强转获取泛型类
        ParameterizedType parameterizedType = (ParameterizedType) type;
        //Class实际也是一个Type，可以直接将Type强转为Class
        Type[] paramTypes = parameterizedType.getActualTypeArguments();
        for (int i = 0; i < paramTypes.length; i++) {
            Class claxx = (Class) paramTypes[i];
            System.out.println("父类泛型值(" + i + ") = " + claxx.getName());
        }

        System.out.println("\n");
        System.out.println("============================================");
        System.out.println("getInterfaces()：获取实现的接口，父类的接口无法获取");
        System.out.println("============================================");
        Class[] interfacesClass = clazz.getInterfaces();
        for (int i = 0; i < interfacesClass.length; i++) {
            Class claxx = interfacesClass[i];
            System.out.println("实现的接口(" + i + ") = " + claxx.getName());
        }

        System.out.println("\n");
        System.out.println("============================================");
        System.out.println("getPackage()：获取所在的包");
        System.out.println("============================================");
        Package aPackage = clazz.getPackage();
        System.out.println("getPackage() = " + aPackage.getName());

        System.out.println("\n");
        System.out.println("============================================");
        System.out.println("getAnnotations()：获取类注解");
        System.out.println("============================================");
        Annotation[] annotations = clazz.getAnnotations();
        for (int i = 0; i < annotations.length; i++) {
            Annotation annotation = annotations[i];
            System.out.println("People类注解(" + i + ") = " + annotation.toString());
        }
    }
}
