package com.pc.reflect;

import com.pc.reflect.bean.Person;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * @author panchao
 * @create 2019/4/18 17:03
 * @Describe
 *      反射 - 基本使用
 */
public class Test1 implements ITest {

    @Override
    public void test(){
        System.out.println("====================================");
        System.out.println("常规初始化对象");
        System.out.println("====================================");
        Person p = new Person();
        p.setName("zhangsan");
        p.setAge(20);
        System.out.println("常规new对象 = " + p.toString());

        System.out.println("\n\n");
        System.out.println("====================================");
        System.out.println("通过反射初始化对象");
        System.out.println("====================================");
        try {
            Person person = Person.class.newInstance();

            //getDeclaredField是可以获取一个类的所有字段.
            //name属性是private
            Field f_name = Person.class.getDeclaredField("name");
            f_name.setAccessible(true);
            f_name.set(person, "zhangsan");

            //getDeclaredField是可以获取一个类的所有字段.
            //age属性是private
            Field f_age = Person.class.getDeclaredField("age");
            f_age.setAccessible(true);
            f_age.set(person, 30);

            //sex属性是public
            //getField只能获取类的public字段.
            Field f_sex = Person.class.getField("sex");
            f_sex.set(person, false);

            Method m_show = Person.class.getMethod("show");
            m_show.invoke(person);

            Method m_display = Person.class.getMethod("display", String.class);
            m_display.invoke(person, "China");

            System.out.println("反射newInstance()对象 = " + person.toString());
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
    }

}
