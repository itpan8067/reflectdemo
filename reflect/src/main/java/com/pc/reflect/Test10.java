package com.pc.reflect;

import com.pc.reflect.bean.People;
import com.pc.reflect.bean.Person;
import com.pc.reflect.bean.VipUser;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.lang.reflect.TypeVariable;
import java.util.List;
import java.util.Map;

/**
 * @author panchao
 * @create 2019/4/22 19:19
 * @Describe
 *      反射 -- 泛型
 */
public class Test10 implements ITest {

    public Map<? extends Integer, ? super String> map;

    @Override
    public void test() {
        testParameterizedType();

        testClass();
    }

    private void testClass() {
        try {
            System.out.println("==============================================");
            System.out.println("泛型类测试");
            System.out.println("==============================================");
            System.out.println("如何获取泛型类实现类对应传入的Field/Method的具体类型？");
            Type userClassType = VipUser.class.getGenericSuperclass();
            System.out.println("1. 获取VipUser的父类User。vipUserClassType ：VipUser.class.getGenericSuperclass() = " + userClassType);
            if (userClassType instanceof ParameterizedType) {
                Type rawType = ((ParameterizedType) userClassType).getRawType();
                System.out.println("\tUser userClassType.getRawType() " + rawType);

                Class userClass = (Class) ((ParameterizedType) userClassType).getRawType();
                System.out.println("2. User userClassType.getRawType()其实就是User类，可以直接强转为Class = " + userClass);
                System.out.println("\tuserClassType.getRawType()必须强制转换为Class，才能获取相应Filed值，Type类型没有Field获取方法");
                Field dataField = userClass.getDeclaredField("data");
                Type dataGenericType = dataField.getGenericType();
                System.out.println("3. 获取data属性的Field的类型。VipUser dataField.getGenericType() = " + dataGenericType + "(泛型类需要使用)");
                Type dataType = dataField.getType();
                System.out.println("\tUser dataField.getType() = " + dataType + "(对于泛型类不可取)");

                System.out.println("4. 遍历User类的所有类型参数，并找到与data泛型属性类型相同的索引");
                int index = 0;
                TypeVariable[] typeVariables = userClass.getTypeParameters();
                for (int i = 0; i < typeVariables.length; i++) {
                    TypeVariable typeVariable = typeVariables[i];
                    System.out.println("\tUser typeVariable[" + i + "] = "+ typeVariable);
                    if (dataGenericType.equals(typeVariable)){
                        index = i;
                    }
                }

                Type[] actualTypes =((ParameterizedType) userClassType).getActualTypeArguments();
                System.out.println("5. User data属性相同的泛型索引找到了！是第[" + index + "]个泛型， 该属性泛型类型 = " + actualTypes[index]);

                System.out.println("\tUser 遍历一下getActualTypeArguments()再次验证一下");
                for (int i = 0; i < actualTypes.length; i++) {
                    Type type = actualTypes[i];
                    System.out.println("\tUser type[" + i + "] = "+ type);
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void testParameterizedType() {
        try {
            System.out.println("==============================================");
            System.out.println("泛型Method参数的用法");
            System.out.println("==============================================");
            Method setInfoMethod = Test10.class.getDeclaredMethod("setInfo", Map.class, List.class);
            setInfoMethod.setAccessible(true);

            // getGenericParameterTypes()：返回的是参数的参数化的类型,里面的带有实际的参数类型
            // 比如：paramType = java.util.Map<java.lang.String, com.pc.reflect.bean.People>
            Type[] setInfoTypes = setInfoMethod.getGenericParameterTypes();

            // getParameterTypes()：返回是参数的类型，没有带实际的参数类型
            // 比如：paramType = interface java.util.Map
//            Type[] setInfoTypes = setInfoMethod.getParameterTypes();

            for (int i = 0; i < setInfoTypes.length; i++) {
                Type paramType = setInfoTypes[i];
                System.out.println("paramType = " + paramType);
                if (paramType instanceof ParameterizedType) {

                    //getOwnerType()：如果这个类型是某个类型所属，获取这个所属者的类型，否则返回null
                    Type ownerType = ((ParameterizedType) paramType).getOwnerType();
                    System.out.println("\townerType = " + ownerType);

                    //getRawType()：获取<>前面的实际类型
                    Type rawType = ((ParameterizedType) paramType).getRawType();
                    System.out.println("\trawType = " + rawType);

                    //getActualTypeArguments()：获取<>中间实际类型
                    Type[] genericTypes = ((ParameterizedType) paramType).getActualTypeArguments();
                    for (int j = 0; j < genericTypes.length; j++) {
                        Type genericType = genericTypes[j];
                        System.out.println("\tgenericType = " + genericType);
                    }
                }
            }

            System.out.println("==============================================");
            System.out.println("泛型Method返回值的用法");
            System.out.println("==============================================");
            Method getInfoMethod = Test10.class.getDeclaredMethod("getInfo");

            // getGenericReturnType()：返回的返回值的类型,里面的带有实际的参数类型
            // 比如：returnType = java.util.Map<com.pc.reflect.bean.People, com.pc.reflect.bean.Person>
            Type returnType = getInfoMethod.getGenericReturnType();

            // getReturnType()：返回的返回值的类型，没有带实际的参数类型
            // 比如：returnType = interface java.util.Map
//            Type returnType = getInfoMethod.getReturnType();

            System.out.println("returnType = " + returnType);
            if (returnType instanceof ParameterizedType) {
                Type ownerType = ((ParameterizedType) returnType).getOwnerType();
                System.out.println("\townerType = " + ownerType);

                Type rawType = ((ParameterizedType) returnType).getRawType();
                System.out.println("\trawType = " + rawType);

                Type[] genericTypes = ((ParameterizedType) returnType).getActualTypeArguments();
                for (int j = 0; j < genericTypes.length; j++) {
                    Type genericType = genericTypes[j];
                    System.out.println("\tgenericType = " + genericType);
                }
            }
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
    }

    private void setInfo(Map<String, People> map, List<Person> list) {
        System.out.println("setInfo()被调用");
    }

    private Map<People, Person> getInfo() {
        System.out.println("getInfo()被调用");
        return null;
    }
}
