package com.pc.reflect.proxy;

/**
 * @author panchao
 * @create 2019/4/22 11:46
 * @Describe
 */
public class HumanUtils {
    public void method1() {
        System.out.println("共用方法1");
    }

    public void method2() {
        System.out.println("共用方法2");
    }
}
