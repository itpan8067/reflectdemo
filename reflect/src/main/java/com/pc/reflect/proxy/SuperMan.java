package com.pc.reflect.proxy;

/**
 * @author panchao
 * @create 2019/4/22 11:43
 * @Describe
 */
public class SuperMan implements IHuman {

    @Override
    public void info() {
        System.out.println("我是超人");
    }

    @Override
    public void fly() {
        System.out.println("我可以飞！");
    }
}
