package com.pc.reflect.proxy;

/**
 * @author panchao
 * @create 2019/4/22 11:42
 * @Describe
 */
public interface IHuman {
    void info();
    void fly();
}
