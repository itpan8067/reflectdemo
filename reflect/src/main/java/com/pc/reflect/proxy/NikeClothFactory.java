package com.pc.reflect.proxy;

/**
 * @author panchao
 * @create 2019/4/22 11:30
 * @Describe
 */
public class NikeClothFactory implements IClothFactoty {
    @Override
    public void productCloth() {
        System.out.println("生产一批耐克衣服！");
    }
}
