package com.pc.reflect.proxy;

/**
 * @author panchao
 * @create 2019/4/22 10:24
 * @Describe
 */
public class RealSubject implements ISubject {
    @Override
    public void action() {
        System.out.println("我们被代理类，记得要执行我哦！");
    }
}
