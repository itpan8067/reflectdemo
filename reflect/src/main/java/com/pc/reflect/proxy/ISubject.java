package com.pc.reflect.proxy;

/**
 * @author panchao
 * @create 2019/4/22 10:24
 * @Describe
 */
public interface ISubject {
    void action();
}

