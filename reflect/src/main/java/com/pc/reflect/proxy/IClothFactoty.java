package com.pc.reflect.proxy;

/**
 * @author panchao
 * @create 2019/4/22 11:28
 * @Describe
 */
public interface IClothFactoty {
    void productCloth();
}
