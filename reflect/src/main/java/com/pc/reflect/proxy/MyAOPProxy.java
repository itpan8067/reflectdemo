package com.pc.reflect.proxy;

import java.lang.reflect.Proxy;

/**
 * @author panchao
 * @create 2019/4/22 11:50
 * @Describe
 */
public class MyAOPProxy {

    public static Object getProxyInstance (Object obj) {
        MyAOPInvocationHandler aopInvocationHandler = new MyAOPInvocationHandler();
        aopInvocationHandler.setObject(obj);

        return Proxy.newProxyInstance(obj.getClass().getClassLoader(), obj.getClass().getInterfaces(), aopInvocationHandler);
    }
}
