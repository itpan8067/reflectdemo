package com.pc.reflect.proxy;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

/**
 * @author panchao
 * @create 2019/4/22 10:26
 * @Describe
 */
public class MyInvocationHandler implements InvocationHandler {

    Object object;

    //1、给被代理对象实例化
    //2、返回一个代理类对象
    public Object build(Object obj) {
        this.object = obj;
        return Proxy.newProxyInstance(obj.getClass().getClassLoader(), obj.getClass().getInterfaces(), this);
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        Object returnVal = method.invoke(object, args);
        return returnVal;
    }
}
