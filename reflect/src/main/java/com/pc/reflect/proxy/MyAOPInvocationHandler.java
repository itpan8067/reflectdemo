package com.pc.reflect.proxy;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

/**
 * @author panchao
 * @create 2019/4/22 11:48
 * @Describe
 */
public class MyAOPInvocationHandler implements InvocationHandler {

    Object object;

    public void setObject(Object obj) {
        this.object = obj;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {

        HumanUtils humanUtils = new HumanUtils();
        humanUtils.method1();

        Object returnVal = method.invoke(object, args);

        humanUtils.method2();
        return returnVal;
    }
}
