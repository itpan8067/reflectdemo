package com.pc.reflect;

import com.pc.reflect.annotation.AnnotationSxtField;
import com.pc.reflect.annotation.AnnotationSxtTable;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;

/**
 * @author panchao
 * @create 2019/4/22 16:22
 * @Describe
 *      反射应用 - 注解
 */
public class Test9 implements ITest {

    @Override
    public void test() {
        try {
            Class clazz = Class.forName("com.pc.reflect.bean.SxtStudent");
            Annotation[] annotations = clazz.getAnnotations();
            for (int i = 0; i < annotations.length; i++) {
                Annotation annotation = annotations[i];
                System.out.println("annotation = " + annotation);
            }

            System.out.println("#####################################");
            System.out.println("#####################################");

            // 获得类的指定的注解
            AnnotationSxtTable sxtTable = (AnnotationSxtTable) clazz.getAnnotation(AnnotationSxtTable.class);
            System.out.println("sxtTable = " + sxtTable.value());

            // 获取类的属性的注解
            Field idField = clazz.getDeclaredField("id");
            Field snameField = clazz.getDeclaredField("studentName");
            Field ageField = clazz.getDeclaredField("age");
            AnnotationSxtField idAnnotationSxtField = idField.getAnnotation(AnnotationSxtField.class);
            System.out.println("idAnnotationSxtField = ["
                    + idAnnotationSxtField.columnName()
                    + "," + idAnnotationSxtField.type()
                    + ", " + idAnnotationSxtField.length()
                    + "]");
            AnnotationSxtField snameAnnotationSxtField = snameField.getAnnotation(AnnotationSxtField.class);
            System.out.println("snameAnnotationSxtField = ["
                    + snameAnnotationSxtField.columnName()
                    + "," + snameAnnotationSxtField.type()
                    + ", " + snameAnnotationSxtField.length()
                    + "]");
            AnnotationSxtField ageAnnotationSxtField = ageField.getAnnotation(AnnotationSxtField.class);
            System.out.println("ageAnnotationSxtField = ["
                    + ageAnnotationSxtField.columnName()
                    + "," + ageAnnotationSxtField.type()
                    + ", " + ageAnnotationSxtField.length()
                    + "]");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }
    }
}
