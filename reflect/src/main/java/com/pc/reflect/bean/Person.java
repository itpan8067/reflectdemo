package com.pc.reflect.bean;

/**
 * @author panchao
 * @create 2019/4/18 17:04
 * @Describe
 */
public class Person {
    private String name;
    private int age;
    public boolean sex;

    public Person() {
    }

    public Person(String name) {
        this.name = name;
    }

    public Person(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void show(){
        System.out.println("我是一个人");
    }

    public void display(String nation) {
        System.out.println("我的国籍是:"+nation);
    }

    @Override
    public String toString() {
        return "Person [name=" + name + ", age=" + age + ", sex=" + sex + "]";
    }
}
