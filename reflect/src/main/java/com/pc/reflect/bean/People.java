package com.pc.reflect.bean;

/**
 * @author panchao
 * @create 2019/4/19 11:26
 * @Describe
 */
@AMyAnnotationRuntime(value = "abcefg")
@AMyAnnotationClass(value =  "123456")
public class People extends Creature<String> implements Comparable, IMyInterface{
    private String name;
    private int age;
    public boolean sex;
    protected long size;
    double color;

    public People() {
    }

    private People(String name) {
        this.name = name;
    }

    public People(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public boolean isSex() {
        return sex;
    }

    public void setSex(boolean sex) {
        this.sex = sex;
    }

    public long getSize() {
        return size;
    }

    public void setSize(long size) {
        this.size = size;
    }

    public double getColor() {
        return color;
    }

    public void setColor(double color) {
        this.color = color;
    }

    @AMyAnnotationRuntime(value = "gfedcba")
    public void show(){
        System.out.println("show()");
    }

    public int add(int i, int j, String value) {
        System.out.println("调用add() - input = " + value);
        return i + j;
    }

    public static void info() {
        System.out.println("调用静态方法info()");
    }

    public void display(String nation) throws Exception {
        System.out.println("display() - 我的国籍是:"+nation);
    }

    private void printf() {
        System.out.println("printf() - 私有打印");
    }

    @Override
    public String toString() {
        return "Person [name=" + name + ", age=" + age + ", sex=" + sex + ", size=" + size + ", color=" + color + "]";
    }

    @Override
    public int compareTo(Object o) {
        return 0;
    }

    class Bird {

    }
}
