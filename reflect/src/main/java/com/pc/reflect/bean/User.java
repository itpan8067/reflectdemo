package com.pc.reflect.bean;

/**
 * @author panchao
 * @create 2019/4/22 20:13
 * @Describe
 */
public class User<T, D> {
    private String name;
    private int age;
    private T data;
    private D message;
}
