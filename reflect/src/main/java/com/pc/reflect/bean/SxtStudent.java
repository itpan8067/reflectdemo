package com.pc.reflect.bean;

import com.pc.reflect.annotation.AnnotationSxtField;
import com.pc.reflect.annotation.AnnotationSxtTable;

/**
 * @author panchao
 * @create 2019/4/22 18:00
 * @Describe
 */
// AnnotationSxtTable注解表示对应ORM中的tb_student表
@AnnotationSxtTable("tb_student")
public class SxtStudent {
    // AnnotationSxtField注解表示对应ORM中的tb_student表中的id字段
    @AnnotationSxtField(columnName = "id", type = "int", length = 10)
    private int id;
    // AnnotationSxtField注解表示对应ORM中的tb_student表中的sname字段
    @AnnotationSxtField(columnName = "sname", type = "varchar", length = 10)
    private String studentName;
    // AnnotationSxtField注解表示对应ORM中的tb_student表中的age字段
    @AnnotationSxtField(columnName = "age", type = "int", length = 3)
    private int age;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getStudentName() {
        return studentName;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}
