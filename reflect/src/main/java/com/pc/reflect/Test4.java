package com.pc.reflect;

import com.pc.reflect.bean.People;

import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

/**
 * @author panchao
 * @create 2019/4/19 11:31
 * @Describe
 *      反射 -- Constructor/Field/Method的使用
 */
public class Test4 implements ITest {

    @Override
    public void test(){
        Class clazz = People.class;

        testField(clazz);
        testMethod(clazz);
        testConstructor(clazz);
    }

    private void testConstructor(Class clazz) {
        int index = 0;

        System.out.println("\n\nConstructor操作");
        System.out.println("============================================");
        System.out.println("getConstructors() : 获取到运行时类声明为public的方法，父类不能获取");
        System.out.println("============================================");
        Constructor[] constructors = clazz.getConstructors();
        for (index = 0; index < constructors.length; index++) {
            Constructor constructor = constructors[index];
            System.out.println("Constructors(" + index + ") = " + constructor.toString());
        }

        System.out.println("============================================");
        System.out.println("getDeclaredConstructors() : 获取到运行时类中所有的方法(public, private, protected)，父类不能获取");
        System.out.println("============================================");
        Constructor[] declaredConstructors = clazz.getDeclaredConstructors();
        for (index = 0; index < declaredConstructors.length; index++) {
            Constructor constructor = declaredConstructors[index];
            System.out.println("DeclaredConstructors(" + index + ") = " + constructor.toString());
        }
    }

    private void testMethod(Class clazz){
        int index = 0;

        System.out.println("\n\nMethod操作");
        System.out.println("============================================");
        System.out.println("getMethods() : 获取到运行时类及其父类(间接父类)声明为public的方法");
        System.out.println("============================================");
        Method[] methods = clazz.getMethods();
        for (index = 0; index < methods.length; index++) {
            Method method = methods[index];
            int mod = method.getModifiers();
            String modString = Modifier.toString(mod);

            Class returnClass = method.getReturnType();
            String returnClassString = returnClass.getSimpleName();

            String nameString = method.getName();

            System.out.println("Method(" + index + ") = " + method.toString()
                    + " -- [方法名：" + nameString
                    + ", 方法修饰符：" + modString
                    + ", 方法返回值类型：" + returnClassString
                    + "]");

            Annotation[] annotations = method.getAnnotations();
            if (annotations != null && annotations.length > 0) {
                System.out.println("注解：");
                for (Annotation annotation : annotations) {
                    System.out.println("\t"+annotation.toString());
                }
            }

            Class[] paramTypeClass = method.getParameterTypes();
            if (paramTypeClass != null && paramTypeClass.length > 0) {
                System.out.println("参数列表：");
                for (Class claxx : paramTypeClass) {
                    System.out.println("\t"+claxx.getName());
                }
            }

            Class[] exceptionClass = method.getExceptionTypes();
            if (exceptionClass != null && exceptionClass.length > 0) {
                System.out.println("异常类型：");
                for (Class claxx : exceptionClass) {
                    System.out.println("\t"+claxx.getName());
                }
            }
        }

        System.out.println("============================================");
        System.out.println("getDeclaredMethods() : 获取到运行时类中所有的方法(public, private, protected)，但不能获取父类的任何方法");
        System.out.println("============================================");
        Method[] declaredMethods = clazz.getDeclaredMethods();
        for (index = 0; index < declaredMethods.length; index++) {
            Method method = declaredMethods[index];
            System.out.println("DeclaredMethod(" + index + ") = " + method.toString());
        }
    }

    private void testField(Class clazz){
        int index = 0;
        System.out.println("Field操作");
        System.out.println("============================================");
        System.out.println("getFields() : 只能获取到运行时类及其父类中声明为public的属性");
        System.out.println("============================================");
        Field[] fields = clazz.getFields();
        for (index = 0; index < fields.length; index++) {
            Field field = fields[index];
            System.out.println("Field(" + index + ") = " + field.toString());
        }

        System.out.println("============================================");
        System.out.println("getDeclaredFields() : 获取到运行时类中所有的属性(public, private, protected)，但不能获取父类的任何属性");
        System.out.println("============================================");
        Field[] declaredFields = clazz.getDeclaredFields();
        for (index = 0; index < declaredFields.length; index++) {
            Field field = declaredFields[index];
            // 获取每个属性的权限修饰符
            int mod = field.getModifiers();
            String modString = Modifier.toString(mod);
            // 获取属性的变量类型
            Class typeClass = field.getType();
            String typeString = typeClass.getSimpleName();
            // 获取属性名
            String nameString = field.getName();

            System.out.println("DeclaredField(" + index + ") = " + field.toString() + " - [属性名：" + nameString + ", 属性修饰符：" + modString + ", 类型：" + typeString + "]");
        }
    }
}
