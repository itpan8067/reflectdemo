package com.pc.reflect;

import com.pc.reflect.bean.Person;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * @author panchao
 * @create 2019/4/18 20:29
 * @Describe
 *      反射 -- ClassLoader使用
 */
public class Test3 implements ITest {

    @Override
    public void test() {
        // System Classloader
        // 负责java-classpath或-D java.class.path所指的目录下的类与jar包装入工作，最常用的加载器
        ClassLoader classLoader = ClassLoader.getSystemClassLoader();
        System.out.println("System Classloader = " + classLoader);

        // Extension Classloader
        // 负责jre/lib/ext目录下的jar包或者-D java.ext.dirs指定目录下的jar包装入工作
        ClassLoader extClassLoader = classLoader.getParent();
        System.out.println("System Classloader Parent（Extension Classloader）= " + extClassLoader);

        // Bootstap Classloader
        // 用C++编写的，是JVM自带的类加载器，负责加载java平台核心库，该加载器无法直接获取
        ClassLoader bootClassLoader = extClassLoader.getParent();
        System.out.println("Extension Classloader Parent（Bootstap Classloader）= " + bootClassLoader);

        ClassLoader personClassLoader = Person.class.getClassLoader();
        System.out.println("自定义Person类加载器(系统加载器) = " + personClassLoader);

        ClassLoader objectClassLoader = Object.class.getClassLoader();
        System.out.println("Object系统库类加载器(获取不到) = " + objectClassLoader);

        //Android里面不行
        ClassLoader classLoader1 = this.getClass().getClassLoader();
        InputStream inputStream = classLoader1.getResourceAsStream("\\src\\main\\java\\com\\example\\cameratest\\test\\jdbc.properties");
        if (inputStream != null) {
            try {
                Properties properties = new Properties();
                properties.load(inputStream);
                String user = properties.getProperty("user");
                String pwd = properties.getProperty("passwd");
                System.out.println("user = " + user + ", passwd = " + pwd);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
