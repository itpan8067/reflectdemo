package com.pc.reflect;

import com.pc.reflect.bean.People;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * @author panchao
 * @create 2019/4/19 19:03
 * @Describe
 *      反射 -- public/private/protected/default的应用
 */
public class Test6 implements ITest {

    @Override
    public void test() {
        System.out.println("Field调用方法:3步");
        System.out.println("\t1. obj = Class.newInstance()");
        System.out.println("\t2. Field field = Class.getField()");
        System.out.println("\t3. field.set(obj, value)");
        testField();

        System.out.println("\n");
        System.out.println("Method调用方法:3步");
        System.out.println("\t1. obj = Class.newInstance()");
        System.out.println("\t2. Method method = Class.getMethod(\"name\", 参数Type列表....)");
        System.out.println("\t3. method.invoke(obj, value列表...)");
        testMethod();

        System.out.println("\n");
        System.out.println("指定构造器调用方法:2步");
        System.out.println("\t1. Constructor constructor = Class.getConstructor(参数Type列表....)");
        System.out.println("\t2. obj = constructor.newInstance(value列表....)");
        System.out.println("\t 注意：一般类没有默认的无参构造器，必须要指定特定的构造器");
        testConstructor();
    }

    private void testConstructor() {
        try {
            Constructor constructor = People.class.getDeclaredConstructor(String.class, int.class);
            constructor.setAccessible(true);
            People people = (People) constructor.newInstance("Jack", 10);
            System.out.println(people);
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
    }


    private void testMethod() {
        try {
            People people = People.class.newInstance();

            System.out.println("============================================");
            System.out.println("public, 无参, 无返回值, protected/default与Field一致");
            System.out.println("============================================");
            Method showMethod = People.class.getMethod("show");
            Object returnVal = showMethod.invoke(people);
            System.out.println("返回值: " + returnVal);

            System.out.println("============================================");
            System.out.println("public, 无参, 无返回值, 静态, protected/default与Field一致");
            System.out.println("============================================");
            Method infoMethod = People.class.getMethod("info");
            Object returnVal1 = infoMethod.invoke(People.class);
            System.out.println("返回值: " + returnVal1);

            System.out.println("============================================");
            System.out.println("public, 有参, 有返回值, protected/default与Field一致");
            System.out.println("============================================");
            Method addMethod = People.class.getMethod("add", int.class, int.class, String.class);
            Object returnVal2 = addMethod.invoke(people, 1, 2, "I Love U");
            System.out.println("返回值: " + returnVal2);

            System.out.println("============================================");
            System.out.println("private, 无参, 无返回值, protected/default与Field一致");
            System.out.println("============================================");
            Method printfMethod = People.class.getDeclaredMethod("printf");
            printfMethod.setAccessible(true);
            Object returnVal3 = printfMethod.invoke(people);
            System.out.println("返回值: " + returnVal3);

        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
    }

    private void testField() {
        try {
            People people = People.class.newInstance();
            System.out.println("============================================");
            System.out.println("public属性:可以使用getField()和getDeclaredField()");
            System.out.println("============================================");
            Field sexField = People.class.getField("sex");
            System.out.println(people);
            sexField.set(people, true);
            System.out.println(people);

            System.out.println("\n");
            System.out.println("============================================");
            System.out.println("private属性:只能使用getDeclaredField(), setAccessible(true)");
            System.out.println("============================================");
            Field nameField = People.class.getDeclaredField("name");
            nameField.setAccessible(true);
            System.out.println(people);
            nameField.set(people, "Jack");
            System.out.println(people);

            System.out.println("\n");

            System.out.println("============================================");
            System.out.println("protected属性:只能使用getDeclaredField(), 如果不在同一个包中,则需要setAccessible(true),同一包中不需要设置");
            System.out.println("============================================");
            Field sizeField = People.class.getDeclaredField("size");
            sizeField.setAccessible(true);
            System.out.println("不同包中protected属性 : " + people);
            sizeField.set(people, 44);
            System.out.println("不同包中protected属性 : " + people);

            Car car = Car.class.newInstance();
            Field runField = Car.class.getDeclaredField("run");
            System.out.println("同一包中protected属性 : " + car);
            runField.set(car, true);
            System.out.println("同一包中protected属性 : " + car);

            System.out.println("\n");
            System.out.println("============================================");
            System.out.println("default属性:只能使用getDeclaredField(), 如果不在同一个包中,则需要setAccessible(true),同一包中不需要设置");
            System.out.println("============================================");
            Field colorField = People.class.getDeclaredField("color");
            colorField.setAccessible(true);
            System.out.println("不同包中default属性 : " + people);
            colorField.set(people, 1.05F);
            System.out.println("不同包中default属性 : " + people);

            Field colorField1 = Car.class.getDeclaredField("color");
            System.out.println("同一包中default属性 : " + car);
            colorField1.set(car, 1.05F);
            System.out.println("同一包中default属性 : " + car);
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        }
    }

}
