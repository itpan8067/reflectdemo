package com.pc.reflect;

/**
 * @author panchao
 * @create 2019/4/19 19:25
 * @Describe
 */
public class Car {
    public String brand;
    private int size;
    double color;
    protected boolean run;

    public Car() {
    }

    public double getColor() {
        return color;
    }

    public void setColor(double color) {
        this.color = color;
    }

    public boolean isRun() {
        return run;
    }

    public void setRun(boolean run) {
        this.run = run;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    @Override
    public String toString() {
        return "Car [brand=" + brand + ", size=" + size + ", color=" + color + ", run=" + run + "]";
    }
}
