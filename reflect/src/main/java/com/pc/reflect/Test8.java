package com.pc.reflect;

import com.pc.reflect.proxy.IClothFactoty;
import com.pc.reflect.proxy.IHuman;
import com.pc.reflect.proxy.MyAOPProxy;
import com.pc.reflect.proxy.NikeClothFactory;
import com.pc.reflect.proxy.SuperMan;

/**
 * @author panchao
 * @create 2019/4/22 11:41
 * @Describe
 *      反射 -- 动态代理应用2
 */
public class Test8 implements ITest {

    @Override
    public void test() {
        SuperMan superMan = new SuperMan();
        IHuman human = (IHuman) MyAOPProxy.getProxyInstance(superMan);
        human.info();
        System.out.println("####################");
        human.fly();

        System.out.println("####################");
        NikeClothFactory nikeClothFactory = new NikeClothFactory();
        IClothFactoty clothFactoty = (IClothFactoty) MyAOPProxy.getProxyInstance(nikeClothFactory);
        clothFactoty.productCloth();
    }
}
