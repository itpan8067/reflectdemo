package com.pc.reflect;

import com.pc.reflect.proxy.IClothFactoty;
import com.pc.reflect.proxy.ISubject;
import com.pc.reflect.proxy.MyInvocationHandler;
import com.pc.reflect.proxy.NikeClothFactory;
import com.pc.reflect.proxy.RealSubject;

/**
 * @author panchao
 * @create 2019/4/19 20:21
 * @Describe
 *      反射 -- 动态代理应用1
 */
public class Test7 implements ITest {

    @Override
    public void test() {
        //1、创建被代理类对象
        RealSubject realSubject = new RealSubject();

        //2、创建实现了InvocationHandler接口类的对象
        MyInvocationHandler invocationHandler = new MyInvocationHandler();

        //3、调用build方法，动态的返回一个同样实现了RealSubject所在类实现的接口ISubject的代理类对象
        ISubject subject = (ISubject) invocationHandler.build(realSubject);

        //转到InvocationHandler实现的invoke()方法
        subject.action();

        //另一个例子
        NikeClothFactory nikeClothFactory = new NikeClothFactory();
        IClothFactoty clothFactoty = (IClothFactoty) invocationHandler.build(nikeClothFactory);
        clothFactoty.productCloth();
    }
}
