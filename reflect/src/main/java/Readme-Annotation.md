# Annotation注解
> JDK5.0引入

## Annotation作用
- 对程序作出解释。注释的功能
- 可以被编译器读取。

## Annotation格式
自定义创建注解格式：
```java
public @interface 注解名 {
    // 1. 每一个方法实际上是声明了一个配置参数；
    // 2. 方法的名称就是参数的名称；
    // 3. 返回值就是参数的类型(返回值只能是基本类型，Class,String, enum)；`String name() default "pc"`中String参数类型。
    // 4. 通过default来设置注解参数的默认值, 通常为空字符或0，-1等；`String name() default "pc"`
    // 5. 如果注解有一个参数，则通常参数名为value；
}
```
使用格式：
```java
// 1个参数时
@注解名(arg=参数值)
或
@注解名(参数值)

// 多个参数时
@注解名(arg1=参数值, arg2=参数值, arg3=｛数组参数值1，数组参数值2，...｝, ...)
```

## Annotation应用范围

- `package`包
- `class`类
- `method`方法
- `field`属性
附加到以上对象，相当于给他们添加额外的辅助信息，可以通过反射机制编程实现对元数据的访问。

Annotation使用步骤：
- 自定义一个注解；
- 类里面使用注解；
- 类里面写一个解析程序读取注解值；

## Annotation内置方法
### 元注解
> 负责注解其他注解。
#### @Target
> 用于描述注解的使用范围

ElementType取值范围：
- PACKAGE：用于修饰package包
- TYPE：用于修饰类，接口，枚举，Annotation类型
- CONSTRUCTOR：用于修饰构造器
- FIELD：用于修饰属性；
- METHOD：用于修饰方法；
- LOCAL_VARIABLE：用于修饰局部变量；
- PARAMETER：用于修饰参数；
```java
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.ANNOTATION_TYPE)
public @interface Target {
    ElementType[] value();
}
```
#### @Retention
> 表示需要在什么级别保存该注释信息，用于描述注解的生命周期。

RetentionPolicy取值范围：
- SOURCE：在源文件中有效(即源文件保留)
- CLASS：在class文件中有效(即class保留)
- RUNTIME：在运行时有效(即运行时保留，反射必不可少)
```java
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.ANNOTATION_TYPE)
public @interface Retention {
    RetentionPolicy value();
}
```
#### @Documented
#### @Inherited

### 常用注解
#### @Override
> 重写父类的方法
```java
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.SOURCE)
public @interface Override {
}
```

#### @Deprecated
> 用于修饰方法，属性，类，表示不鼓励程序员使用。
```java
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(value={CONSTRUCTOR, FIELD, LOCAL_VARIABLE, METHOD, PACKAGE, PARAMETER, TYPE})
public @interface Deprecated {
}
```

#### @SuppressWarnings
> 用于抑制编译时的警告信息
```java
@Target({TYPE, FIELD, METHOD, PARAMETER, CONSTRUCTOR, LOCAL_VARIABLE})
@Retention(RetentionPolicy.SOURCE)
public @interface SuppressWarnings {
    String[] value();
}
```